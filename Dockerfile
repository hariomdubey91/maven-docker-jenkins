FROM tomcat
ARG WORKSPACE
WORKDIR $WORKSPACE
ADD ./webapp/target/webapp.war /usr/local/tomcat/webapps/
EXPOSE 8080
CMD ["catalina.sh", "run"]
